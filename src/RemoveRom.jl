using Base: iscontiguous, Float64
#=
Remove a ROM or Clones of it from a ROM collection
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
# using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Load the module
using RomCleaner
using LightXML


"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
                         "version info, default values, " *
                         "options with types, variable " *
                         "number of arguments.",
                         version = "Version 1.0", # version info
                         add_version = true)      # audo-add version option

    @add_arg_table! s begin
        "roms-dir"
            nargs = 1
            arg_type = String
            help = "Path to the directory containing the ROMs."
            required = true
        "--dat-file"
            nargs = '?'
            arg_type = String
            default = "."
            help = "Path to DAT XML file with ROM information (should be obtained from the emulator web-page)."
            required = true
        "--rom-name"
            nargs = '?'
            arg_type = String
            default = "."
            help = "Name of the ROM, or clone to be removed"
            required = true
        "--dry"
            action = :store_true   # this makes it a flag
            help = "Only show info and the files that will be removed."
        "--remove-clones"
            action = :store_true   # this makes it a flag
            help = "If the ROM is a parent ROM, all of its clones will also be removed."
        "--debug", "-d"
            action = :store_true   # this makes it a flag
            help = "Show debug information."
        end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



""" Check if the ROMs in the list exist, and remove them."""
function remove_roms(romsList::Array{Rom, 1}, romsDir::String; dry::Bool=true, fmt::String="zip")::Array{String, 1}
    @debug "remove_roms :: START" length(romsList) romsDir dry fmt

    tmpRomPath::String = ""
    tmpRomName::String = ""
    toRemove::Array{String, 1} = String[]
    # Check extans of ROM files
    for game in romsList
        tmpRomName = game.name
        tmpRomPath = joinpath(romsDir, "$(tmpRomName).zip")
        # @show tmpRomPath isfile(tmpRomPath)
        if isfile(tmpRomPath)
            push!(toRemove, tmpRomPath)
        end
    end

    # Remove ROMs if not in dry run
    for path in toRemove
        println("$(path)\t$(filesize(path)/1000000) Megabytes")
        if !dryRun
            if isfile(path)
                rm(path, force=false, recursive=false)
            else
                @error "ROM not found in system!" basename(path) path
            end
        end
    end

    return toRemove

end


#####  MAIN  #####
args = get_params(ARGS)
# @show(args)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

romsDir = abspath(args["roms-dir"][1])
datPath = realpath(args["dat-file"])
romName = args["rom-name"]
dryRun = args["dry"]
delClones = args["remove-clones"]
debug = args["debug"]

#=
validDataTypes = ["top-int-double", "top-double-double", "bottom-int-double", "bottom-double-double"]
if dataType ∉ validDataTypes
    @error "The data type is not valid!" dataType validDataTypes
    exit(-7)
end
=#

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "ROM cleaning will be perofmred with the following parameters:" romsDir datPath romName dryRun delClones

# Load DAT file
#=
datInfo = parse_file(datPath)
datRoot = root(datInfo)
println(name(datRoot))
# Get info for all games
gamesInfo = get_elements_by_tagname(datRoot, "game")
=#

gamesInfo = load_dat(datPath)


# ces[2]

romsInfoDict = Dict{String, String}()
romIdx = 1
romFound = false
game, romFound = get_rom_info(romName, gamesInfo)

# Make sure the ROM was found
if !romFound
    @warn "The ROM $(romName) was not found!" romName datPath
end

summary(stdout, game)
# Roms list, including clones if any
gamesList = Array{Rom, 1}([game])

# Check the ROM type
if length(game.cloneof) > 0
    @info "This ROM is clone of $(game.cloneof)" game.name game.parent game.isbios
elseif game.isbios
    @info "This ROM is a BIOS" game.name game.parent game.isbios
else
    @info "This ROM is a parent ROM $(game.parent)" game.name game.parent game.isbios
    # Add the parent ROM and the clones
    clones = get_rom_clones(game.name, gamesInfo)
    for tmpGame in clones
        push!(gamesList, tmpGame)
    end
    @info "$(length(clones)) Clones found for ROM $(game.name) [$(game.description)]"
end

# Create a list of games

# Remove ROMs if needed
println("\nTotal ROMs found:")
for g in gamesList
    println("$(g.name)\t$(g.description)")
end
remove_roms(gamesList, romsDir, dry=dryRun, fmt="zip")
