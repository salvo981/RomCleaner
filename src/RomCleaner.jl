module RomCleaner

include("rom.jl")
include("datparser.jl")
include("systools.jl")



# expose the methods
export Rom,
    summary,
    load_dat,
    get_rom_info,
    get_rom_clones,
    makedir

end # end module

