"""
Represents a ROM as described in DAT file from FinalBurnNeo.
https://github.com/libretro/FBNeo

fields: name, cloneof, romof, isbios.
"""

#= Some examples of content of the GAME tag

Example of clone
    <game name="99lstwarb" cloneof="repulse" romof="repulse">
        <description>'99: The Last War (bootleg) [Bootleg]</description>
        <year>1985</year>
        <manufacturer>bootleg</manufacturer>

Example of ROM requirng a BIOS.
In this case the field 'romof' is present even if it is the parent ROM
    <game name="bjourney" romof="neogeo">
        <description>Blue's Journey / Raguy (ALM-001)(ALH-001)</description>
        <year>1990</year>
        <manufacturer>Alpha Denshi Co.</manufacturer>
        <rom name="022-p1.p1" size="1048576" crc="6a2f6d4a"/>
        ...
        <rom name="sfix.sfix" merge="sfix.sfix" size="131072" crc="c2ea0cfd"/>
        <rom name="000-lo.lo" merge="000-lo.lo" size="131072" crc="5a86cff2"/>
        <video orientation="horizontal" width="320" height="224" aspectx="4" aspecty="3"/>
        <driver status="good"/>
    </game>

# Example of BIOS ROM
    <game isbios="yes" name="neogeo">
        <description>Neo Geo [BIOS only]</description>
        <year>1990</year>
        <manufacturer>SNK</manufacturer>
        <rom name="sp-s3.sp1" size="131072" crc="91b64be3"/>
        <rom name="uni-bios_2_0.rom" size="131072" crc="0c12c2ad"/>
        <rom name="uni-bios_1_3.rom" size="131072" crc="b24b44a0"/>
        ...
        <rom name="sfix.sfix" size="131072" crc="c2ea0cfd"/>
        <rom name="000-lo.lo" size="131072" crc="5a86cff2"/>
    </game>

=#


struct Rom
    name::String
    cloneof::String
    romof::String
    sampleof::String # name of sample file
    isbios::Bool
    description::String
    year::String
    manufacturer::String
    driverStatus::String
    parent::String
    clones::Array{String, 1}

    # constructor, make sure the values are positive
    function Rom(name::String, cloneof::String, romof::String, sampleof::String, isbios::Bool; description::String="", year::String="?", manufacturer::String="", driverStatus::String="", parent::String="", clones::Array{String, 1}=[])
        # make sure that the service name is valid
        new(name, cloneof, romof, sampleof, isbios, description, year, manufacturer, driverStatus, parent, clones)
    end
end



"""
Overload summary and show functions for Point
"""
function Base.summary(io::IO, r::Rom)
    println(io, "ROM Info\nROM -> $(r.name)\nDescription -> $(r.description)\nYear -> $(r.year)\nManufacturer -> $(r.manufacturer)")
end

Base.show(io::IO, r::Rom) = summary(io, r)
