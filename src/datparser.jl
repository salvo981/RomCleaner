"""
Function to lad nad parse DAT files
https://github.com/libretro/FBNeo
"""

using LightXML
using RomCleaner


"""Load DAT"""
function load_dat(datPath::String)::Array{XMLElement, 1}
    @debug "load_dat :: START" datPath
    datInfo::XMLDocument = parse_file(datPath)
    # Get info for all games
    gamesInfo::Array{XMLElement, 1} = get_elements_by_tagname(root(datInfo), "game")

    return gamesInfo
end



"""Obtain multiple info on a specific ROM"""
function get_rom_info(romName::String, gamesInfo::Array{XMLElement, 1})::Tuple{Rom, Bool}
    @debug "get_rom_info :: START" romName

    romsInfoDict::Dict{String, String} = Dict{String, String}()
    romIdx::Int = 1
    romFound::Bool = false
    # Struct that will be returned
    game = Rom(romName, "", "", "", false, description="", year="?", manufacturer="", driverStatus="", parent="", clones=String[])
    
    
    for (i, rom) in enumerate(gamesInfo)
        # println(i)
        # extract the dictionary with roms info
        romsInfoDict = attributes_dict(rom)
        
        if romsInfoDict["name"] == romName
            romFound = true
            romIdx = i
            sampleName::String = ""
            parentName::String = ""
            romOfName::String = ""
            # Extract descrition
            description = content(find_element(rom, "description"))
            manufacturer = content(find_element(rom, "manufacturer"))
            year = content(find_element(rom, "year"))
            # Check if it is a BIOS
            if haskey(romsInfoDict, "isbios")
                game = Rom(romName, "", "", "", true, description=description, year="?", manufacturer=manufacturer, driverStatus="", parent=romName, clones=String[])
            else # It is a parent or clone ROM
                # Extract sample name if Any
                if haskey(romsInfoDict, "sampleof")
                    sampleName = romsInfoDict["sampleof"]
                end
                # Exact romof feild
                if haskey(romsInfoDict, "romof")
                    romOfName = romsInfoDict["romof"]
                end
                
                # If it is a clone ROM
                if haskey(romsInfoDict, "cloneof")
                    parentName = romsInfoDict["cloneof"]
                    game = Rom(romName, parentName, romOfName, sampleName, false, description=description, year=year, manufacturer=manufacturer, driverStatus="", parent=parentName, clones=String[])
                    # If it is a main ROM
                else
                    game = Rom(romName, "", romOfName, sampleName, false, description=description, year=year, manufacturer=manufacturer, driverStatus="", parent=parentName, clones=String[])
                end
                
            end
            
            break
        end
        
    end
    
    # Return a Rom object which is almost empty
    # summary(stdout, game)
    return (game, romFound)

end



"""Obtain all clones of a parent ROM"""
function get_rom_clones(parentRom::String, gamesInfo::Array{XMLElement, 1})::Array{Rom, 1}
    @debug "get_rom_clones :: START" parentRom

    cloneName::String = ""
    tmpCloneName::String = ""
    # Struct that will be returned
    game = Rom(parentRom, "", "", "", false, description="", year="?", manufacturer="", driverStatus="", parent="", clones=String[])
    romClones::Array{Rom, 1} = Array{Rom, 1}()
    # cloneNames::Array{String, 1} = String[]

    for (i, rom) in enumerate(gamesInfo)
        # Only consider entries which are clones
        if attribute(gamesInfo[i], "cloneof") !== nothing
            cloneName = attribute(gamesInfo[i], "cloneof")
            # Found a clone of the ROM
            if cloneName == parentRom
                romOfName::String = ""
                if attribute(gamesInfo[i], "romof") !== nothing
                    romOfName = attribute(gamesInfo[i], "romof")
                end
                sampleName::String = ""
                if attribute(gamesInfo[i], "sampleof") !== nothing
                    sampleName = attribute(gamesInfo[i], "sampleof")
                end

                # cloneNames::Array{String, 1} = String[]
                tmpCloneName = attribute(gamesInfo[i], "name")
                # push!(cloneNames, tmpCloneName)
                # Extract other info
                attribute(find_element(gamesInfo[5], "driver"), "status")
                description::String = content(find_element(rom, "description"))
                manufacturer::String = content(find_element(rom, "manufacturer"))
                year::String = content(find_element(rom, "year"))
                driver::String = attribute(find_element(rom, "driver"), "status")
                # println(parentRom)
                game = Rom(tmpCloneName, parentRom, romOfName, sampleName, false, description=description, year=year, manufacturer=manufacturer, driverStatus=driver, parent=parentRom, clones=String[])
                push!(romClones, game)
                # summary(stdout, game)
            end
        end
    end
    
    # for el in cloneNames
    #     println(el)
    # end
    

    # Return a Rom object which is almost empty
    # summary(stdout, game)
    return romClones

end
